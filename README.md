# @b08/functional, seeded from @b08/library-seed, library type: dry
A set of 'functional programming' conversions

# mapWith
Creates a function that maps array with provided mapper function.

```
function itemMapper(a: number): string { // return new value … }

// usually is written like this
function arrayMapper(a: number[]): string[] { return a.map(doSmth); }

// can now write it like this
const arrayMapper = mapWith(itemMapper);
```

When the mapper function has several arguments, first parameter is element of the array, the rest of the parameters are the same for each array element.
```
function itemMapper(a: number, b: number): string { // return new value … }

// usually is written like this
function arrayMapper(a: number[], b: number): string[] { return a.map(a1 => itemMapper(a1, b)); }

// alternatively
const arrayMapper = mapWith(itemMapper);

// or just call directly
const result: string[] = mapWith(itemMapper)([1, 2, 3], 4); 
```

# filterWith
Creates a function that filters the array with provided predicate.

```
function predicate(a: number, b: number): boolean { // return bool … }

// usually is written like this
function arrayFilter(a: number[], b: number): number[] { return a.filter(a1 => predicate(a1, b)); }

// alternatively
const arrayFilter = filterWith(predicate);

// or just call directly
const result: number[] = filterWith(predicate)([1, 2, 3], 4);
```

# filterWithNot / rejectWith
Same as filterWith, but inverts predicate result.

# partial

## Where it is applicable.
Sometimes you have to use functions having 2 arguments in a one-argument context.
for example, here employeeMatches is used like that

```
export function findEmployeeById(employees: Employee[], id: EmployeeId): Employee {
	return employees.find(employee => employeeMatches(employee, id));
}

function employeeMatches(employee: Employee, id: EmployeeId): boolean {
	return employeeIdEquals(employee.id, id);
}
```

## Suggestion 
is to shorten partial application a bit, just like that:
```

export function findEmployeeById(employees: Employee[], id: EmployeeId): Employee {
	// instead of this
	// return employees.find(employee => employeeMatches(employee, id));
	// write this
	   return employee.find(partial(employeeMatches, id));
	// or this
	   return employee.find(partial2(employeeMatches, id));
}
```

Here "partial2", means that you fix the target function to having 2 parameters exactly. As well as "partial3" is fixed to 3 and "partial4" to 4 correspondingly.
And "partial" has overrides for any amount of arguments in target function up to 4, which is a bit less strict, but it has limitation, you can't predefine all arguments.
I could not make safe typing for that case.


## Alternatively
Can use lodash _.partialRight, which is a bit longer definition, larger package, and yes, has that defect with unsafe typing for no-arguments return function. 