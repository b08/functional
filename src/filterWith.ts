export function filterWith<T1, TRest extends any[]>(predicate: (a: T1, ...b: TRest) => boolean): (a: T1[], ...b: TRest) => T1[] {
  return function (a: T1[] = [], ...b: TRest): T1[] {
    return a.filter(item => predicate(item, ...b));
  };
}

export function filterWithNot<T1, TRest extends any[]>(predicate: (a: T1, ...b: TRest) => boolean): (a: T1[], ...b: TRest) => T1[] {
  return function (a: T1[], ...b: TRest): T1[] {
    return a.filter(item => !predicate(item, ...b));
  };
}

export const rejectWith = filterWithNot;
