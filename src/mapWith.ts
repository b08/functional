export function mapWith<T1, TRest extends any[], TR>(mapper: (a: T1, ...b: TRest) => TR): (a: T1[], ...b: TRest) => TR[] {
  return function (a: T1[] = [], ...b: TRest): TR[] {
    return a.map(item => mapper(item, ...b));
  };
}
