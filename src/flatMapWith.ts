export function flatMapWith<T1, TRest extends any[], TR>(mapper: (a: T1, ...b: TRest) => TR[]): (a: T1[], ...b: TRest) => TR[] {
  return function (a: T1[] = [], ...b: TRest): TR[] {
    return a.reduce((res, item) => {
      const result = mapper(item, ...b);
      if (Array.isArray(result) && result.length) { res.push(...result); }
      return res;
    }, []);
  };
}
