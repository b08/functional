
// 2 => 1
export function rPartial<TR, TI1, TI2>(target: (i1: TI1, i2: TI2) => TR, i2: TI2): (i1: TI1) => TR;
// 3 => 1
export function rPartial<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i2: TI2, i3: TI3): (i1: TI1) => TR;
// 3 => 2
export function rPartial<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i3: TI3): (i1: TI1, i2: TI2) => TR;
// 4 => 1
export function rPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i2: TI2, i3: TI3, i4: TI4): (i1: TI1) => TR;
// 4 => 2
export function rPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i3: TI3, i4: TI4): (i1: TI1, i2: TI2) => TR;
// 4 => 3
export function rPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i4: TI4): (i1: TI1, i2: TI2, i3: TI3) => TR;

export function rPartial<TR>(target: (...args: any[]) => TR, ...lastArgs: any[]): (...args: any[]) => TR {
  return function (...firstArgs: any[]): TR {
    return target(...firstArgs, ...lastArgs);
  };
}
