import { describe } from "@b08/test-runner";
import { mapWith } from "../src";

describe("mapWith", it => {
  it("should map function with one argument", async expect => {
    // arrange
    const mapper = (n: number) => n + 1;

    const testParams = [1, 2, 3];
    const expected = [2, 3, 4];

    // act
    const result = mapWith(mapper)(testParams);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should map function with two argument", async expect => {
    // arrange
    const mapper = (n: number, n1: number) => n + n1;

    const testParams = [1, 2, 3];
    const expected = [3, 4, 5];

    // act
    const result = mapWith(mapper)(testParams, 2);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should work with void function, just in case", async expect => {
    // arrange
    function mapper(n: number, n1: number): void { return; }

    const testParams = [1, 2, 3];
    const expected = [undefined, undefined, undefined];

    // act
    const result = mapWith(mapper)(testParams, 2);

    // assert
    expect.deepEqual(result, expected);
  });
});
