import { test } from "@b08/test-runner";
import { filterWith, rejectWith } from "../src";

test("filterWith should work with one argument predicate", async expect => {
  // arrange
  const predicate = (n: number) => n > 2;

  const testParams = [1, 2, 3, 4];
  const expected = [3, 4];

  // act
  const result = filterWith(predicate)(testParams);

  // assert
  expect.deepEqual(result, expected);
});

test("filterWith should work with two argument predicate", async expect => {
  // arrange
  const predicate = (n: number, m: number) => m > n;

  const testParams = [1, 2, 3, 4];
  const expected = [1, 2];

  // act
  const result = filterWith(predicate)(testParams, 3);

  // assert
  expect.deepEqual(result, expected);
});

test("rejectWith should work with two argument predicate", async expect => {
  // arrange
  const predicate = (n: number, m: number) => m > n;

  const testParams = [1, 2, 3, 4];
  const expected = [3, 4];

  // act
  const result = rejectWith(predicate)(testParams, 3);

  // assert
  expect.deepEqual(result, expected);
});
