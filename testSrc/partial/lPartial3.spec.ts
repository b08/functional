import { describe } from "@b08/test-runner";
import { lPartial } from "../../src";

const source3 = (i1: number, i2: number, i3: number) => i1 + i2 * 2 + i3 * 3;

describe("lPartial3", it => {
  it("should return partial1 function that sums 3 numbers in correct order", expect => {
    // arrange

    // act
    const result = lPartial(source3, 2, 4)(1);

    // assert
    expect.equal(result, 13);
  });

  it("should return partial2 function that sums 3 numbers in correct order", expect => {
    // arrange

    // act
    const result = lPartial(source3, 4)(1, 2);

    // assert
    expect.equal(result, 12);
  });
});
