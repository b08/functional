import { describe } from "@b08/test-runner";
import { lPartial } from "../../src";

const source4 = (i1: number, i2: number, i3: number, i4: number) => i1 + i2 * 2 + i3 * 3 + i4 * 4;

describe("lPartial4", it => {
  it("should return partial1 function that sums 4 numbers in correct order", expect => {
    // arrange

    // act
    const result = lPartial(source4, 1, 2, 3)(4);

    // assert
    expect.equal(result, 30);
  });

  it("should return partial2 function that sums 4 numbers in correct order", expect => {
    // arrange

    // act
    const result = lPartial(source4, 1, 2)(3, 4);

    // assert
    expect.equal(result, 30);
  });

  it("should return partial3 function that sums 4 numbers in correct order", expect => {
    // arrange

    // act
    const result = lPartial(source4, 1)(2, 3, 4);

    // assert
    expect.equal(result, 30);
  });
});
