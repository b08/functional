import { describe } from "@b08/test-runner";
import { rPartial } from "../../src";

const source4 = (i1: number, i2: number, i3: number, i4: number) => i1 + i2 * 2 + i3 * 3 + i4 * 4;

describe("rPartial4", it => {
  it("should return partial1 function that sums 4 numbers in correct order", async expect => {
    // arrange

    // act
    const result = rPartial(source4, 2, 3, 4)(1);

    // assert
    expect.equal(result, 30);
  });

  it("should return partial2 function that sums 4 numbers in correct order", async expect => {
    // arrange

    // act
    const result = rPartial(source4, 3, 4)(1, 2);

    // assert
    expect.equal(result, 30);
  });

  it("should return partial3 function that sums 4 numbers in correct order", async expect => {
    // arrange

    // act
    const result = rPartial(source4, 4)(1, 2, 3);

    // assert
    expect.equal(result, 30);
  });
});
