import { describe } from "@b08/test-runner";
import { lPartial3 } from "../../src";

const source3 = (i1: number, i2: number, i3: number) => i1 + i2 * 2 + i3 * 3;

describe("lPartialNum3", it => {
  it("should return partial0 function that sums 3 numbers in correct order", async expect => {
    // arrange

    // act
    const result = lPartial3(source3, 1, 2, 3)();

    // assert
    expect.equal(result, 14);
  });

  it("should return partial1 function that sums 3 numbers in correct order", async expect => {
    // arrange

    // act
    const result = lPartial3(source3, 1, 2)(4);

    // assert
    expect.equal(result, 17);
  });

  it("should return partial2 function that sums 3 numbers in correct order", async expect => {
    // arrange

    // act
    const result = lPartial3(source3, 1)(2, 4);

    // assert
    expect.equal(result, 17);
  });
});
