import { describe } from "@b08/test-runner";
import { lPartial1, lPartial2 } from "../../src";

const source1 = i => i;
const source2 = (i1: number, i2: number) => i1 + i2 * 2;

describe("lPartialNum1", it => {
  it("should return partial0 function that returns a number", async expect => {
    // arrange

    // act
    const result = lPartial1(source1, 1)();

    // assert
    expect.equal(result, 1);
  });
});

describe("rPartialNum2", it => {
  it("should return partial0 function that sums 2 numbers in correct order", async expect => {
    // arrange

    // act
    const result = lPartial2(source2, 1, 2)();

    // assert
    expect.equal(result, 5);
  });

  it("should return partial1 function that sums 2 numbers in correct order", async expect => {
    // arrange

    // act
    const result = lPartial2(source2, 1)(2);

    // assert
    expect.equal(result, 5);
  });
});


