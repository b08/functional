import { describe } from "@b08/test-runner";
import { rPartial } from "../../src";

const source2 = (i1: number, i2: number) => i1 + i2 * 2;

describe("rPartial2", it => {
  it("should return partial1 function that sums 2 numbers in correct order", async expect => {
    // arrange

    // act
    const result = rPartial(source2, 2)(1);

    // assert
    expect.equal(result, 5);
  });
});


