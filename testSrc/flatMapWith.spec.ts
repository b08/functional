import { describe } from "@b08/test-runner";
import { flatMapWith } from "../src";

describe("flatMapWith", it => {
  it("should map function with one argument", async expect => {
    // arrange
    const mapper = (n: number) => [n, n + 1];

    const testParams = [1, 4];
    const expected = [1, 2, 4, 5];

    // act
    const result = flatMapWith(mapper)(testParams);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should map function with two argument", async expect => {
    // arrange
    const mapper = (n: number, n1: number) => [n, n1];

    const testParams = [1, 7];
    const expected = [1, 5, 7, 5];

    // act
    const result = flatMapWith(mapper)(testParams, 5);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should work with empty function", async expect => {
    // arrange
    function mapper(n: number, n1: number): [] { return null; }

    const testParams = [1, 2, 3];
    const expected = [];

    // act
    const result = flatMapWith(mapper)(testParams, 2);

    // assert
    expect.deepEqual(result, expected);
  });
});
